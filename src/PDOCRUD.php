<?php
namespace PDOCRUD;

use PDO;
use PDOException;

class PDOCRUD
{
    public $db;

    public function __construct($hostname = null, $database = null, $username = null, $password = null)
    {
        $this->hostname = $hostname;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
    }

    public function connect()
    {
        if (!$this->db instanceof PDO) {
            try {
                if (empty($this->hostname)
                 || empty($this->database)
                 || empty($this->username)) {
                    throw new PDOException(
                        'Missing at least one PDOConnection(hostname, database, username, password) '.
                        'construct connection parameters.'
                    );
                }
                $this->db = new PDO(
                    "mysql:host=" . $this->hostname .";dbname=".$this->database,
                    $this->username,
                    $this->password,
                    array(
                        PDO::ATTR_EMULATE_PREPARES => false,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    )
                );
            } catch (PDOException $e) {
                throw $e;
            }
        }
    }

    /**
    * Create a value into a table
    * @access public
    * @param string $table
    * @param array $values
    * @return int The last Insert Id on success or throw PDOexeption on failure
    */
    public function create($table, $values)
    {
        try {
            $this->connect();

            $fieldnames = array_keys($values[0]);
            $fields 	= '(`' . implode('` ,`', $fieldnames) . '`)';
            $bounds 	= '(:' . implode(', :', $fieldnames) . ')';
            $sql 		= "INSERT INTO $table $fields VALUES $bounds";

            /** prepare and execute **/
            $stmt = $this->db->prepare($sql);
            foreach ($values as $vals) {
                $stmt->execute($vals);
            }
            return $this->db->lastInsertId();
        } catch (PDOException $e) {
            //if not unique constraint error
            //if ($e->getCode() != 23000){
            throw $e;
            //}
        }
    }

    /**
    * Alias of create
    * @see $this->create();
    */
    public function insert($table, $values)
    {
        try {
            return $this->create($table, $values);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
    * Read values from table
    * @access public
    * @param string $table The name of the table
    * @param string $fieldname
    * @param string $id
    * @return array on success or throw PDOException on failure
    */
    public function read($table, $fieldname = null, $id = null)
    {
        try {
            $this->connect();

            if ($fieldname == null && $id == null) {
                $sql = "SELECT * FROM `$table`";
                $stmt = $this->db->prepare($sql);
            } else {
                $sql = "SELECT * FROM `$table` WHERE `$fieldname`=:id";
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':id', $id);
            }

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
    * Alias of read
    * @see $this->create();
    */
    public function select($table, $fieldname = null, $id = null)
    {
        try {
            return $this->read($table, $fieldname, $id);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
     * Update a value in a table.
     * @access public
     * @param string $table
     * @param string $fieldname, The field to be updated
     * @param string $value The new value
     * @param string $pk The primary key
     * @param string $id The id
     * @throws PDOException on failure
    */
    public function update($table, $fieldname, $value, $pk, $id)
    {
        try {
            $this->connect();

            $sql = "UPDATE LOW_PRIORITY `$table` SET `$fieldname`=:value WHERE `$pk` = :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':value', $value, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }
        return true;
    }

    /**
     * Delete a record from a table.
     * @access public
     * @param string $table
     * @param string $fieldname
     * @param string $id
     * @throws PDOexception on failure
     * */
    public function delete($table, $fieldname, $id)
    {
        try {
            $this->connect();

            $sql = "DELETE FROM `$table` WHERE `$fieldname` = :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }
        return true;
    }

}