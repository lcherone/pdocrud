# PDO CRUD#

Is a helper PDO connection class...

Add the following to your projects composer to bring in the code:

    {
        "require": {
            "lcherone/pdocrud": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:lcherone/pdocrud.git"
            }
        ]
    }


###Usage###

    <?php
    require 'vendor/autoload.php';

    use PDOCRUD/PDOCRUD as PDOConnection;

    $db = new PDOConnection('127.0.0.1', 'database_name', 'lcherone', 'passwort');

    //$db->create($table, $values) //has alias insert()

    //$db->read($table, $fieldname = null, $id = null)  //has alias select()

    //$db->update($table, $fieldname, $value, $pk, $id)

    //$db->delete($table, $fieldname, $id)
    ?>